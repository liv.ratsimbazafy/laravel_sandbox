<?php

namespace App\Filters\Course;

use App\Filters\Course\{AccessFilter, DifficultyFilter, Ordering\ViewOrder};
use App\Filters\FiltersAbstract;


class CourseFilters extends FiltersAbstract
{
    protected $filters = [
        'access' => AccessFilter::class,
        'difficulty' => DifficultyFilter::class,
        'type' => TypeFilter::class,
        'subject' => SubjectFilter::class,
        'started' => StartedFilter::class,
        'views' => ViewOrder::class
    ];

}
