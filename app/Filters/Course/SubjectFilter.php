<?php

namespace App\Filters\Course;


use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

/**
 * @desc This is responsible for filter that depends on relational models
 */
class SubjectFilter extends FilterAbstract
{

    public function filter(Builder $builder, $value)
    {
        return $builder->whereHas('subjects', function (Builder $builder) use ($value) {
            $builder->where('slug', $value);
        });
    }
}
