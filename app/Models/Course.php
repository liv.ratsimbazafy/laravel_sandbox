<?php

namespace App\Models;

use App\Filters\Course\CourseFilters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $appends = ['started'];

    protected $hidden = [
        'users'
    ];


    public function scopeFilter(Builder $builder, $request, array $filters = [])
    {
        return (new CourseFilters($request))->add($filters)->filter($builder);
    }


    public function subjects()
    {
        return $this->morphToMany(Subject::class, 'subjectables');
    }


    public function users()
    {
        return $this->belongsToMany(User::class);
    }


    public function getStartedAttribute()
    {
        if (!auth()->check()) {
            return false;
        }

        return $this->users->contains(auth()->user());
    }
}
