<?php

use App\Http\Controllers\Course\CourseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});

Route::get('/virtual', function () {
    return view('virtual');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*----- courses routes -----*/
Route::get('/courses', [CourseController::class, 'index'])->name('course.all');
